import React from "react";
import NewsPage from "./components/NewsPage";

function App() {
  return (
    <div className="container test">
      <NewsPage />
    </div>
  );
}

export default App;
