import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBolt } from "@fortawesome/free-solid-svg-icons";
import ReactPlayer from "react-player";

export class NewsPage extends Component {
  render() {
    return (
      <>
        <div className="top-container page-title mb-4 ">STORMING NEWS</div>

        <div className="top-container cursor-pointer w-20 page-filters inline-flex">
          <img className="mr-2" src={require("../../images/icons/filter.svg")} />
          <span>FILTERS</span>
        </div>

        <div class="card-container justify-center flex">
          <div className="flex flex-wrap justify-start">
            <div className="card-layout mb-4 bg-white">
              <div className="card">
                <div
                  className="card-top"
                  style={{
                    backgroundImage:
                      "url(" +
                      "https://images.pexels.com/photos/34153/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350" +
                      ")"
                  }}
                >
                  <div class="flex items-start h-86">
                    <div class="flex-1">
                      <div class="flex items-center">
                        <FontAwesomeIcon icon={faBolt} className="bolt" />
                        <div className="ml-1">pinned</div>
                      </div>
                    </div>
                    <div class="flex-1 text-right card-top-date">today</div>
                  </div>
                  <div class="flex items-end h-86">
                    <div class="flex-1">the verge</div>
                  </div>
                </div>
                <div className="card-bottom">Enchance your life by having a sense of purpose</div>
              </div>
              <div className="card-tags">#technology #strategy #action</div>
            </div>

            <div className="card-layout mb-4 bg-white">
              <div className="card">
                <div
                  className="card-top"
                  style={{
                    backgroundImage:
                      "url(" +
                      "https://images.pexels.com/photos/34153/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350" +
                      ")"
                  }}
                >
                  <div class="flex items-start h-86">
                    {/* <div class="flex-1">
                      <div class="flex items-center">
                        <FontAwesomeIcon icon={faBolt} className="bolt" />
                        <div className="ml-1">pinned</div>
                      </div>
                    </div> */}
                    <div class="flex-1 text-right card-top-date">today</div>
                  </div>
                  <div class="flex items-end h-86">
                    <div class="flex-1">the verge</div>
                  </div>
                </div>
                <div className="card-bottom">Enchance your life by having a sense of purpose</div>
              </div>
              <div className="card-tags">#technology #strategy #action</div>
            </div>

            <div className="card-layout mb-4 bg-white">
              <div className="card">
                <div className="card-top-video">
                  <ReactPlayer
                    width="100%"
                    height="182px"
                    light
                    url="https://www.youtube.com/watch?v=K_PA6VNpejQ"
                    config={{
                      youtube: {
                        playerVars: { modestbranding: 1 }
                      }
                    }}
                  />
                </div>
                <div className="card-bottom">Enchance your life by having a sense of purpose</div>
              </div>
              <div className="card-tags">#technology #strategy #action</div>
            </div>

            <div className="card-layout mb-4 bg-white">
              <div className="card-full">
                <div className="card-top">
                  <div class="flex items-start">
                    <div class="flex-1 card-top-date">the verge</div>
                    <div class="flex-1 text-right card-top-date">today</div>
                  </div>
                </div>
                <div className="card-full-body">
                  <p className="card-bottom">Enchance your life by having a sense of purpose</p>
                  <p className="card-full-article">
                    While it was just a TV show, that little speech at the beginning of the original
                    Star Trek show really did do a good job of capturing our feelings about space.
                    It is those feelings that drive our love of astronomy and our desire to learn
                    more and more about it. love of astronomy, maybe unlike any other force on
                    earth, has brought together mankind toward that common goal of conquering the
                    universe. The quest to establish an international space station and to cooperate
                    on spreading our reach off of this planet seems to find commonality between
                    nations that otherwise cannot get along on the surface of the earth. That alone
                    may be a reason that we must continue to support astronomy locally and the space
                    program nationally. It is something that seems to bring peace rather than war
                    and make us a better people. But more than that it is as though this is what we
                    were created to do. To reach out to the stars may be our destiny. If so then our
                    love of astronomy is more than a hobby, it’s a calling.
                  </p>
                </div>
              </div>
              <div className="card-tags">#technology #strategy #action</div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default NewsPage;
